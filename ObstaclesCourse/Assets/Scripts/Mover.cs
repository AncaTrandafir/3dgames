﻿using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] float moveSpeed = 10f;       // serializeField - make it available in Unity

    // Start and Update are automatically called by Unity; = callbacks

    // Start is called before the first frame update
    void Start()
    {
        PrintInstructions();
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
    }

    void PrintInstructions()
    {
        // Print to console
        Debug.Log("Welcome to the Game.");
        Debug.Log("Move your player with WASD or arraw keys.");
        Debug.Log("Don't hit the walls");
    }

    void MovePlayer()
    {
        // Time.deltaTime - how long it takes for each frame to execute, independent of FPS of computer
        // => frame rate independent
        float xValue = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        float zValue = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        // update 60 times per sec
        transform.Translate(xValue, 0, zValue);
    }
}
