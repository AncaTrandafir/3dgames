﻿using UnityEngine;

public class ObjectHit : MonoBehaviour      // obstacle
{
    private void OnCollisionEnter(Collision collision)      // callback
    {
        if (collision.gameObject.tag == "Player")
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
            gameObject.tag = "Hit";
        }
    }
}
